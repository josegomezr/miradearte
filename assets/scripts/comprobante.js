var facturador = {
    detalle: {
        igv:      0,
        total:    0,
        subtotal: 0,
        valord: 0,
        cliente_id: 0,
        vendedor: $("#vendedor").val( ),
        estado_f: $("#estado_f").val( ),
        fecha: $("#fecha").val( ),
        tipo_p: $("#tipo_p").val( ),
        factura: $("input[name='factura']").val( ),
        items:    []
    },

    /* Encargado de agregar un producto a nuestra colección */
    registrar: function(item)
    {
        /* Agregamos el total */ 
        item.total = (item.cantidad * item.precio);
        item.total = item.total + parseInt(item.valord);
        this.detalle.items.push(item);

        this.refrescar();
    },

    /* Encargado de actualizar el precio/cantidad de un producto */
    actualizar: function(id, row)
    {
        /* Capturamos la fila actual para buscar los controles por sus nombres */
        row = $(row).closest('.list-group-item');

        /* Buscamos la columna que queremos actualizar */
        $(this.detalle.items).each(function(indice, fila){
            if(indice == id)
            {
                /* Agregamos un nuevo objeto para reemplazar al anterior */
                facturador.detalle.items[indice] = {
                    producto_id: row.find("input[name='producto_id']").val(),
                    producto: row.find("input[name='producto']").val(),
                    cantidad: row.find("input[name='cantidad']").val(),
                    valord: row.find("input[name='valord']").val(),
                    precio: row.find("input[name='precio']").val()
                   
                };

                facturador.detalle.items[indice].total = facturador.detalle.items[indice].precio *
                                                         facturador.detalle.items[indice].cantidad;
                facturador.detalle.items[indice].total = facturador.detalle.items[indice].total +
                                                         parseInt(facturador.detalle.items[indice].valord);                                                                                  

                return false;
            }
        })

        this.refrescar();
    },

    /* Encargado de retirar el producto seleccionado */
    retirar: function(id)
    {
        /* Declaramos un ID para cada fila */
        $(this.detalle.items).each(function(indice, fila){
            if(indice == id)
            {
                facturador.detalle.items.splice(id, 1);
                return false;
            }
        })

        this.refrescar();
    },

    /* Refresca todo los productos elegidos */
    refrescar: function()
    {
        this.detalle.total = 0;

        /* Declaramos un id y calculamos el total */
        $(this.detalle.items).each(function(indice, fila){
            facturador.detalle.items[indice].id = indice;
            facturador.detalle.total += fila.total;
        })

        /* Calculamos el subtotal e IGV */
        this.detalle.igv      = (this.detalle.total * 0.12).toFixed(2); // 12 % El IVA y damos formato a 2 deciamles
        this.detalle.subtotal = (this.detalle.total - this.detalle.igv).toFixed(2); // Total - IVA y formato a 2 decimales
        this.detalle.total    = this.detalle.total.toFixed(2);

        var template   = $.templates("#facturador-detalle-template");
        var htmlOutput = template.render(this.detalle);

        $("#facturador-detalle").html(htmlOutput);
    }
};



$(document).ready(function(){
    $("#btn-agregar").click(function(){
        facturador.registrar({
            producto_id: $("#producto_id").val(),
            producto: $("#producto").val(),
            cantidad: $("#cantidad").val(),
            valord: $("#valord").val(),
            precio:   $("#precio").val()
            
        });
        $("#producto").val('');
        $("#precio").val('');
        $("#cantidad").val('');
        $("#valord").val('');

    })
    
    $("#frm-comprobante").submit(function(){
        
        var form = $(this);
        
        if(facturador.detalle.cliente_id == 0)
        {
            alert('Debe agregar un cliente');
        }
        else if(facturador.detalle.items.length == 0)
        {
            alert('Debe agregar por lo menos un detalle al comprobante');
        }else
        {
            $.ajax({
                dataType: 'JSON',
                type: 'POST',
                url: form.attr('action'),
                data: facturador.detalle,
                success: function (r) {
                    if(r) window.location.href = '?c=Comprobante';
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(errorThrown + ' ' + textStatus);
                }   
            });            
        }
    
        return false;
    })
    
    /* Autocomplete de cliente, jquery UI */
    $("#ruc").autocomplete({
        dataType: 'JSON',
        source: function (request, response) {
            jQuery.ajax({
                url: '?c=Comprobante&a=ClienteBuscar',
                type: "post",
                dataType: "json",
                data: {
                    criterio: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.RUC,
                            direccion: item.Direccion,
                            cliente: item.Nombre+" "+ item.apellido,
                            telefono: item.tlf,
                            email: item.email
                        }
                    }))
                }
            })
        },
        select: function (e, ui) {
            $("#ruc_id").val(ui.item.id);
            $("#direccion").val(ui.item.direccion);
            $("#cliente").val(ui.item.cliente);
            $("#telefono").val(ui.item.telefono);
            $("#email").val(ui.item.email);
            $(this).blur();
            
            facturador.detalle.cliente_id = ui.item.id;
        }
    })
    
    /* Autocomplete de producto, jquery UI */
    $("#producto").autocomplete({
        dataType: 'JSON',
        source: function (request, response) {
            jQuery.ajax({
                url: '?c=Comprobante&a=ProductoBuscar',
                type: "post",
                dataType: "json",
                data: {
                    criterio: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.Nombre +" "+ item.color,
                            precio: item.Precio,
                            cantidad: item.cantidad,
                            color: item.color
                            
                        }
                    }))
                }
            })
        },
        select: function (e, ui) {
            $("#producto_id").val(ui.item.id);
            $("#color").val(ui.item.color);
            $("#precio").val(ui.item.precio);
            $("#cantidad").val(ui.item.cantidad);
            $("#cantidad").focus();
        }
    })
})