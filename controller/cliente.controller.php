<?php
require_once 'model/cliente.entidad.php';
require_once 'model/cliente2.model.php';

class ClienteController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new ClienteModel();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/cliente/cliente.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Cliente();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/cliente/cliente-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Listar()
    {
        print_r($this->model->Listar());
    }
    
    public function Guardar(){
        $alm = new Cliente();
        
        $alm->__SET('id',              $_REQUEST['id']);
        $alm->__SET('RUC',          $_REQUEST['RUC']);
        $alm->__SET('Nombre',          $_REQUEST['Nombre']);
        $alm->__SET('apellido',       $_REQUEST['apellido']);
        $alm->__SET('tlf',           $_REQUEST['tlf']);
        $alm->__SET('email',            $_REQUEST['email']);
        $alm->__SET('Direccion',       $_REQUEST['Direccion']);
        

        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Cliente');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}