<?php
require_once 'model/inventario.entidad.php';
require_once 'model/inventario.model.php';

class InventarioController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new InventarioModel();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/inventario/inventario.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Inventario();
        
        /*if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }*/
        
        require_once 'view/header.php';
        require_once 'view/inventario/inventario-registro.php';
        require_once 'view/footer.php';
    }
    public function CrudEntrada(){
        $alm = new Inventario();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/inventario/inventario-editar-entrada.php';
        require_once 'view/footer.php';
    }
    public function CrudSalida(){
        $alm = new Inventario();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/inventario/inventario-editar-salida.php';
        require_once 'view/footer.php';
    }
    public function Listar()
    {
        print_r($this->model->Listar());
    }
    
    public function Guardar(){
        $alm = new Inventario();
        
        $alm->__SET('id',               $_REQUEST['id']);
        $alm->__SET('Nombre',           $_REQUEST['Nombre']);
        $alm->__SET('cantidad',         $_REQUEST['cantidad']);
        $alm->__SET('f_entrada',        $_REQUEST['f_entrada']);
        $alm->__SET('color',            $_REQUEST['color']);
        $alm->__SET('descripcion',      $_REQUEST['descripcion']);
        $alm->__SET('Precio',           $_REQUEST['Precio']);
        $alm->__SET('precio_c',         $_REQUEST['precio_c']);
        $alm->__SET('nfactura',         $_REQUEST['nfactura']);
        $alm->__SET('proveedor',        $_REQUEST['proveedor']);

        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Inventario');
    }
    public function GuardarSalida(){
        $alm = new Inventario();
        
        $alm->__SET('id',               $_REQUEST['id']);
        $alm->__SET('Nombre',           $_REQUEST['Nombre']);
        $alm->__SET('cantidad',         $_REQUEST['cantidad']);
        $alm->__SET('f_salida',         $_REQUEST['f_salida']);
        $alm->__SET('color',            $_REQUEST['color']);
        $alm->__SET('descripcion',      $_REQUEST['descripcion']);
        $alm->__SET('Precio',           $_REQUEST['Precio']);
        $alm->__SET('precio_c',         $_REQUEST['precio_c']);

        if($alm->__GET('id') != '' ? 
           $this->model->ActualizarSalida($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Inventario');
    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}