<?php
require_once 'model/usuario.entidad.php';
require_once 'model/usuario.model.php';

class UsuarioController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new UsuarioModel();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/usuario/usuario.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Usuario();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/usuario/usuario-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Listar()
    {
        print_r($this->model->Listar());
    }
    
    public function Guardar(){
        $alm = new Usuario();
        
        $alm->__SET('id',               $_REQUEST['id']);
        $alm->__SET('cedula',           $_REQUEST['cedula']);
        $alm->__SET('nombre',           $_REQUEST['nombre']);
        $alm->__SET('apellido',         $_REQUEST['apellido']);
        $alm->__SET('password',         $_REQUEST['password']);
        $alm->__SET('passwordr',        $_REQUEST['passwordr']);
        $alm->__SET('fecha',            $_REQUEST['fecha']);
        $alm->__SET('nivel',            $_REQUEST['nivel']);

        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Usuario');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}