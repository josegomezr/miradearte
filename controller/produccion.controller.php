<?php
require_once 'model/produccion.model.php';
require_once 'model/produccion.entidad.php';
require_once 'model/producto.model.php';
require_once 'model/cliente.model.php';

class ProduccionController{
    
    private $model;
    private $pmodel;
    private $cmodel;
    
    public function __CONSTRUCT(){
        $this->model  = new ProduccionModel();
        $this->pmodel = new ProductoModel();
        $this->cmodel = new ClienteModel();
    }
    
    
    public function Index(){
        
        require_once 'view/header.php';
        require_once 'view/produccion/produccion.php';
        require_once 'view/footer.php';
        
    }
    public function Crud(){
        require_once 'view/header.php';
        require_once 'view/produccion/editar.php';
        require_once 'view/footer.php';
    }
    
    public function Ver(){
        
        $comprobante = $this->model->Obtener($_REQUEST['id']);
        
        require_once 'view/header.php';
        require_once 'view/produccion/ver.php';
        require_once 'view/footer.php';
    }
    public function Ver2(){
        
        $comprobante = $this->model->Obtener($_REQUEST['id']);
        
        require_once 'view/header.php';
        require_once 'view/produccion/ver2.php';
        require_once 'view/footer.php';
    }
    
    public function Guardar2()
    {
        $alm = new Produccion();
        
        $alm->__SET('id',              $_REQUEST['id']);
        $alm->__SET('id2',              $_REQUEST['id2']);
        $alm->__SET('descripcion',     $_REQUEST['descripcion']);
        $alm->__SET('estado',          $_REQUEST['estado']);
        $alm->__SET('foto',            $_REQUEST['foto']);
        /*if( !empty( $_FILES['foto']['name'] ) ){
            $foto = date('ymdhis') . '-' . strtolower($_FILES['foto']['name']);
            move_uploaded_file ($_FILES['foto']['tmp_name'], 'uploads/' . $foto);
            
        $alm->__SET('foto', $foto);
        }*/

        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Produccion');


    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: ?=produccion');
    }
    
    public function ClienteBuscar()
    {
        print_r(json_encode(
            $this->cmodel->Buscar($_REQUEST['criterio'])
        ));
    }
    
    public function ProductoBuscar()
    {
        print_r(json_encode(
            $this->pmodel->Buscar($_REQUEST['criterio'])
        ));
    }
    
    public function Listar()
    {
        print_r($this->model->Listar());  
    }
    
}