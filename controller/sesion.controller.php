<?php
require_once 'model/sesion.entidad.php';
require_once 'model/sesion.model.php';

class SesionController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new SesionModel();
    }
    
    public function Index(){
        
        require_once 'view/sesion/sesion.php';
        
    }
    
    public function Crud(){
        $alm = new Sesion();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/sesion/sesion-editar.php';
        require_once 'view/footer.php';
    }
    
   
    
    public function Iniciar(){
        $alm = new Sesion();
        
        $alm->__SET('cedula',          $_REQUEST['cedula']);
        $alm->__SET('password',          $_REQUEST['password']);

       $this->model->Comprobar($alm);
        
       
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}