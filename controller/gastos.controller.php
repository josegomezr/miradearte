<?php
require_once 'model/gastos.entidad.php';
require_once 'model/gastos.model.php';

class GastosController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new GastosModel();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/gastos/gastos.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Gastos();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/gastos/gastos-registro.php';
        require_once 'view/footer.php';
    }
       public function Listar()
    {
        print_r($this->model->Listar());
    }
    
    public function Guardar(){
        $alm = new gastos();
        
        $alm->__SET('id',               $_REQUEST['id']);
        $alm->__SET('rif',              $_REQUEST['rif']);
        $alm->__SET('cliente',          $_REQUEST['cliente']);
        $alm->__SET('nfactura',         $_REQUEST['nfactura']);
        $alm->__SET('fecha',            $_REQUEST['fecha']);
        $alm->__SET('total',            $_REQUEST['total']);
        $alm->__SET('tipopago',         $_REQUEST['tipopago']);
        $alm->__SET('clasificacion',    $_REQUEST['clasificacion']);
        $alm->__SET('descripcion',      $_REQUEST['descripcion']);
        
        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Gastos');
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}