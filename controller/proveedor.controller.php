<?php
require_once 'model/proveedor.entidad.php';
require_once 'model/proveedor.model.php';

class ProveedorController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new ProveedorModel();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/proveedor/proveedor.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Proveedor();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/proveedor/proveedor-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Listar()
    {
        print_r($this->model->Listar());
    }
    
    public function Guardar(){
        $alm = new Proveedor();
        
        $alm->__SET('id',              $_REQUEST['id']);
        $alm->__SET('rif',             $_REQUEST['rif']);
        $alm->__SET('nombreempresa',   $_REQUEST['nombreempresa']);
        $alm->__SET('direccion',       $_REQUEST['direccion']);
        $alm->__SET('tlfempresa',      $_REQUEST['tlfempresa']);
        $alm->__SET('emailempresa',    $_REQUEST['emailempresa']);
        $alm->__SET('cedula',          $_REQUEST['cedula']);
        $alm->__SET('nombrecontacto',  $_REQUEST['nombrecontacto']);
        $alm->__SET('apellidocontacto',  $_REQUEST['apellidocontacto']);
        $alm->__SET('tlfcontacto',     $_REQUEST['tlfcontacto']);
        $alm->__SET('emailcontacto',   $_REQUEST['emailcontacto']);

        if($alm->__GET('id') != '' ? 
           $this->model->Actualizar($alm) : 
           $this->model->Registrar($alm));
        
        header('Location: ?c=Proveedor');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        echo json_encode(true);
    }
}