-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2015 a las 19:37:49
-- Versión del servidor: 10.0.17-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `factura`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificacion`
--

CREATE TABLE `clasificacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clasificacion`
--

INSERT INTO `clasificacion` (`id`, `nombre`) VALUES
(1, 'Servicios'),
(2, 'Bienes Muebles'),
(3, 'Bienes Inmuebles'),
(4, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `RUC` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tlf` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `Direccion` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `RUC`, `Nombre`, `apellido`, `tlf`, `email`, `Direccion`) VALUES
(2, '19239552', 'Anthony', 'anthon', '2523522', 'vggfgfgf', 'Caiguire calle el refugio'),
(3, '19239551', 'Henry', 'velasquez', '6554', 'henry', 'el peñon'),
(4, '19239553', 'juan', 'mata', '5666', 'elcid19@gmail.com', 'jhjhj'),
(5, '20347249', 'astrid', 'mendez', '04120124712', 'adm@gmail.com', 'vargas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobante`
--

CREATE TABLE `comprobante` (
  `id` int(11) NOT NULL,
  `Cliente_id` int(1) NOT NULL,
  `vendedor` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_p` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `estado_f` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `IGV` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SubTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comprobante`
--

INSERT INTO `comprobante` (`id`, `Cliente_id`, `vendedor`, `fecha`, `tipo_p`, `estado_f`, `IGV`, `SubTotal`, `Total`) VALUES
(67, 3, 'josefa', '', '', '', '120.00', '880.00', '1000.00'),
(71, 3, 'josefa', '2015-12-04', 'efectivo', 'cancelado', '2958.00', '21692.00', '24650.00'),
(72, 3, 'josefa', '2015-12-04', 'efectivo', 'cancelado', '12018.00', '88132.00', '100150.00'),
(73, 3, 'josefa', '2015-12-04', 'efectivo', 'cancelado', '12018.00', '88132.00', '100150.00'),
(74, 2, 'josefa', '2015-12-04', 'efectivo', 'cancelado', '96.00', '704.00', '800.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobante_detalle`
--

CREATE TABLE `comprobante_detalle` (
  `Comprobante_id` int(11) NOT NULL,
  `Producto_id` int(11) NOT NULL,
  `Cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `PrecioUnitario` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comprobante_detalle`
--

INSERT INTO `comprobante_detalle` (`Comprobante_id`, `Producto_id`, `Cantidad`, `PrecioUnitario`, `Total`) VALUES
(67, 4, '2.00', '500.00', '1000.00'),
(71, 4, '1.00', '500.00', '500.00'),
(71, 8, '2.00', '12000.00', '24150.00'),
(72, 4, '100.00', '500.00', '50150.00'),
(73, 4, '100.00', '500.00', '50150.00'),
(73, 5, '500.00', '100.00', '50000.00'),
(74, 7, '5.00', '130.00', '800.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(255) NOT NULL,
  `rif` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `cliente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `nfactura` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `total` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tipopago` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `clasificacion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `rif`, `cliente`, `nfactura`, `fecha`, `total`, `tipopago`, `clasificacion`, `descripcion`) VALUES
(3, '19239551', 'henry', '65446', '2015-12-03', '100', 'Efectivo', 'Servicios', 'holaaa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `cantidad` varchar(25) NOT NULL,
  `f_entrada` varchar(25) NOT NULL,
  `f_salida` varchar(25) NOT NULL,
  `color` varchar(25) NOT NULL,
  `descripcion` varchar(25) NOT NULL,
  `precio_v` varchar(25) NOT NULL,
  `precio_c` varchar(25) NOT NULL,
  `proveedor` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `nombre`, `cantidad`, `f_entrada`, `f_salida`, `color`, `descripcion`, `precio_v`, `precio_c`, `proveedor`) VALUES
(1, 'taza', '100', '2015-12-02', '', 'verdes', '', '120', '100', 'geeksystemve');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produccion`
--

CREATE TABLE `produccion` (
  `id` int(255) NOT NULL,
  `cedula` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tlf` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `foto` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `produccion`
--

INSERT INTO `produccion` (`id`, `cedula`, `nombre`, `apellido`, `tlf`, `direccion`, `email`, `foto`) VALUES
(1, '19239551', 'Henry', 'velasquez', '6554', 'el peñon', 'henry@fsdfd', '151204071555-images.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produccion_detalle`
--

CREATE TABLE `produccion_detalle` (
  `id` int(255) NOT NULL,
  `produccion_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `producto` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `produccion_detalle`
--

INSERT INTO `produccion_detalle` (`id`, `produccion_id`, `producto`, `cantidad`, `descripcion`) VALUES
(1, '1', 'tasa verdes', '', ''),
(2, '1', 'tasa azul', '', ''),
(3, '1', '', '100.00', ''),
(4, '1', '', '500.00', ''),
(5, '1', '', '', 'Escriba la descripción del producto'),
(6, '1', '', '', 'Escriba la descripción del producto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Precio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cantidad` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `f_entrada` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `f_salida` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `color` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `precio_c` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `proveedor` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `Nombre`, `Precio`, `cantidad`, `f_entrada`, `f_salida`, `color`, `descripcion`, `precio_c`, `proveedor`) VALUES
(4, 'tasa', '500.00', '0', '2015-12-02', '', 'verdes', 'ljajsañl', '450', 'geeksystemve'),
(5, 'tasa', '100.00', '0', '2015-12-02', '', 'azul', 'jjhj', '80', 'geeksystemve'),
(6, 'tasa', '100.00', '60', '2015-12-02', '', 'rojo', '', '80', 'geeksystemve'),
(7, 'zapato', '130.00', '5', '2015-12-02', '', 'morado verdoso', 'talla 40', '100', 'geeksystemve'),
(8, 'Tazas Brillantes', '12000.00', '50', '2015-12-01', '', 'Doradas', '', '10000', 'geeksystemve');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `rif` varchar(25) NOT NULL,
  `nombreempresa` varchar(25) NOT NULL,
  `direccion` varchar(600) NOT NULL,
  `tlfempresa` varchar(25) NOT NULL,
  `emailempresa` varchar(25) NOT NULL,
  `cedula` varchar(25) NOT NULL,
  `nombrecontacto` varchar(25) NOT NULL,
  `apellidocontacto` varchar(25) NOT NULL,
  `tlfcontacto` varchar(25) NOT NULL,
  `emailcontacto` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `rif`, `nombreempresa`, `direccion`, `tlfempresa`, `emailempresa`, `cedula`, `nombrecontacto`, `apellidocontacto`, `tlfcontacto`, `emailcontacto`) VALUES
(1, 'j-40507684-4', 'geeksystemve', 'calle vargas', '04248545841', 'geeksystemve@gmail.com', '18418281', 'Anthony', 'Anton', '02934318004', 'anthonyanton01@gmail.com'),
(2, 'J-20305480-2', 'Construmar', 'calle mar', '02934563232', 'construmar@gmail.com', '15963235', 'josefita', 'camacho', '04145639856', 'josefitacamacho@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `cedula` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `passwordr` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `nivel` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `cedula`, `nombre`, `apellido`, `password`, `passwordr`, `fecha`, `nivel`) VALUES
(5, '19239551', 'Henry', 'Apellido', '123', '123', '2015-12-04', 'Administrador'),
(7, '19239552', 'juan', 'lopez', '123', '123', '2015-12-04', 'Vendedor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clasificacion`
--
ALTER TABLE `clasificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_comprobante_cliente` (`Cliente_id`);

--
-- Indices de la tabla `comprobante_detalle`
--
ALTER TABLE `comprobante_detalle`
  ADD KEY `FK_comprobante_detalle_comprobante` (`Comprobante_id`),
  ADD KEY `FK_comprobante_detalle_producto` (`Producto_id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `produccion`
--
ALTER TABLE `produccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `produccion_detalle`
--
ALTER TABLE `produccion_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clasificacion`
--
ALTER TABLE `clasificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `produccion`
--
ALTER TABLE `produccion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `produccion_detalle`
--
ALTER TABLE `produccion_detalle`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD CONSTRAINT `FK_comprobante_cliente` FOREIGN KEY (`Cliente_id`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comprobante_detalle`
--
ALTER TABLE `comprobante_detalle`
  ADD CONSTRAINT `FK_comprobante_detalle_comprobante` FOREIGN KEY (`Comprobante_id`) REFERENCES `comprobante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_comprobante_detalle_producto` FOREIGN KEY (`Producto_id`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
