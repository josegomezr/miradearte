<?php
require_once 'lib/anexgrid.php';

class InventarioModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = new PDO('mysql:host=localhost;dbname=factura;charset=utf8', 'root', '');
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		        
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /* Anex Grid */
            $anexgrid = new AnexGrid();

            /* Si es que hay filtro, tenemos que crear un WHERE dinámico */
		    $wh = "id > 0";
		    
		    foreach($anexgrid->filtros as $f)
		    {
		 
		        if($f['columna'] == 'Nombre') $wh .= " AND Nombre LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'cantidad') $wh .= " AND cantidad LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'f_entrada') $wh .= " AND f_entrada LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'f_salida') $wh .= " AND f_salida LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'color') $wh .= " AND color LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'Precio') $wh .= " AND Precio LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'precio_c') $wh .= " AND precio_c LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'nfactura') $wh .= " AND nfactura LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'proveedor') $wh .= " AND proveedor LIKE '%" . addslashes ($f['valor']) . "%'";


		        
		    }
            /* Los registros */
            
            $sql = "
                SELECT * FROM producto
                WHERE $wh ORDER BY $anexgrid->columna $anexgrid->columna_orden
                LIMIT $anexgrid->pagina, $anexgrid->limite
            ";

			$stm = $this->pdo->prepare( $sql );
			$stm->execute();
            
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            
            /* El total de registros */
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM producto
                WHERE $wh
            ")->fetchObject()->Total;
            

			return $anexgrid->responde($result, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM producto WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Inventario();

			$alm->__SET('id', $r->id);
			$alm->__SET('Nombre', $r->Nombre);
            $alm->__SET('cantidad', $r->cantidad);
			$alm->__SET('f_entrada', $r->f_entrada);
			$alm->__SET('f_salida', $r->f_salida);
			$alm->__SET('color', $r->color);
			$alm->__SET('descripcion', $r->descripcion);
			$alm->__SET('Precio', $r->Precio);
			$alm->__SET('precio_c', $r->precio_c);
			$alm->__SET('nfactura', $r->nfactura);
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM producto WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Inventario $data)
	{
		try 
		{
			$sql = "UPDATE producto SET
						 
						Nombre          = ?, 
						cantidad   		= cantidad + ?,
						f_entrada		= ?,
						descripcion		= ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('Nombre'),
					$data->__GET('cantidad'), 
					$data->__GET('f_entrada'),
					$data->__GET('descripcion'),
					$data->__GET('id')
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ActualizarSalida(Inventario $data)
	{
		try 
		{
			$sql = "UPDATE producto SET
						 
						Nombre          = ?, 
						cantidad   		= cantidad - ?,
                        f_salida        = ?,
                        descripcion     = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('Nombre'),
					$data->__GET('cantidad'),
                    $data->__GET('f_salida'),
                    $data->__GET('descripcion'),
					$data->__GET('id')
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Inventario $data)
	{
		try 
		{
		$sql = "INSERT INTO producto (Nombre,cantidad,f_entrada,color,descripcion,Precio,precio_c,nfactura,proveedor) 
		        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
			array(
					$data->__GET('Nombre'),
					$data->__GET('cantidad'),
					$data->__GET('f_entrada'),
					$data->__GET('color'),
                    $data->__GET('descripcion'),
                    $data->__GET('Precio'),
                    $data->__GET('precio_c'),
                    $data->__GET('nfactura'),
                    $data->__GET('proveedor')
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}