<?php
require_once 'lib/anexgrid.php';

class ProveedorModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();		        
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /* Anex Grid */
            $anexgrid = new AnexGrid();

            /* Si es que hay filtro, tenemos que crear un WHERE dinámico */
		    $wh = "id > 0";
		    
		    foreach($anexgrid->filtros as $f)
		    {
		    	if($f['columna'] == 'cedula') $wh .= " AND cedula LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'nombre') $wh .= " AND CONCAT(nombre, ' ', apellidos) LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'cargo') $wh .= " AND cargo LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'sexo' && $f['valor'] != '') $wh .= " AND sexo = '" . addslashes ($f['valor']) . "'";
		        
		    }
            /* Los registros */
            
            $sql = "
                SELECT * FROM proveedor
                WHERE $wh ORDER BY $anexgrid->columna $anexgrid->columna_orden
                LIMIT $anexgrid->pagina, $anexgrid->limite
            ";

			$stm = $this->pdo->prepare( $sql );
			$stm->execute();
            
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            
            /* El total de registros */
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM proveedor
                WHERE $wh
            ")->fetchObject()->Total;
            

			return $anexgrid->responde($result, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM proveedor WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Proveedor();

			$alm->__SET('id', $r->id);
			$alm->__SET('rif', $r->rif);
			$alm->__SET('nombreempresa', $r->nombreempresa);
			$alm->__SET('direccion', $r->direccion);
            $alm->__SET('tlfempresa', $r->tlfempresa);
			$alm->__SET('emailempresa', $r->emailempresa);
			$alm->__SET('cedula', $r->cedula);
			$alm->__SET('nombrecontacto', $r->nombrecontacto);
			$alm->__SET('apellidocontacto', $r->apellidocontacto);
			$alm->__SET('tlfcontacto', $r->tlfcontacto);
			$alm->__SET('emailcontacto', $r->emailcontacto);
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM proveedor WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Proveedor $data)
	{
		try 
		{
			$sql = "UPDATE proveedor SET
						rif			      = ?, 
						nombreempresa     = ?, 
						direccion         = ?,
						tlfempresa        = ?,
						emailempresa      = ?, 
						cedula		      = ?,
                        nombrecontacto    = ?,
                        apellidocontacto  = ?,
                        tlfcontacto       = ?,
                        emailcontacto     = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('rif'),
					$data->__GET('nombreempresa'), 
					$data->__GET('direccion'),
					$data->__GET('tlfempresa'), 
					$data->__GET('emailempresa'),
					$data->__GET('cedula'),
                    $data->__GET('nombrecontacto'),
                    $data->__GET('apellidocontacto'),
					$data->__GET('tlfcontacto'),
					$data->__GET('emailcontacto'),
					$data->__GET('id')
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Proveedor $data)
	{
		try 
		{
		$sql = "INSERT INTO proveedor (rif,nombreempresa,direccion,tlfempresa,emailempresa,cedula,nombrecontacto,apellidocontacto,tlfcontacto,emailcontacto) 
		        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
			array(
					$data->__GET('rif'),
					$data->__GET('nombreempresa'), 
					$data->__GET('direccion'),
					$data->__GET('tlfempresa'), 
					$data->__GET('emailempresa'),
					$data->__GET('cedula'),
                    $data->__GET('nombrecontacto'),
                    $data->__GET('apellidocontacto'),
					$data->__GET('tlfcontacto'),
					$data->__GET('emailcontacto')
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}