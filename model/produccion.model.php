<?php
require 'lib/anexgrid.php';

class ProduccionModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
            $this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
           
            /* Instanciamos AnexGRID */
            $anexGrid = new AnexGrid();
            
            $wh = "id > 0";
            
            foreach($anexGrid->filtros as $f)
            {
                if($f['columna'] == 'Cliente_id') $wh .= " AND  LIKE '%" . addslashes ($f['valor']) . "%'";                
            }

            /* Contamos los registros*/
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM comprobante WHERE $wh
            ")->fetchObject()->Total;

            /* Nuestra consulta dinámica */
            $registros = $this->pdo->query("
                SELECT * FROM comprobante WHERE $wh
                ORDER BY $anexGrid->columna $anexGrid->columna_orden
                LIMIT $anexGrid->pagina,$anexGrid->limite")->fetchAll(PDO::FETCH_ASSOC
             );

            foreach($registros as $k => $r)
            {
                /* Traemos los clientes que tiene asignado cada comprobante */
                $cliente = $this->pdo->query("SELECT * FROM cliente c WHERE c.id = " . $r['Cliente_id'])
                                ->fetch(PDO::FETCH_ASSOC);

                $registros[$k]['Cliente'] = $cliente;
                
                /* Traemos el detalle */
                $registros[$k]['Detalle'][] = $this->pdo->query("SELECT * FROM comprobante_detalle cd WHERE cd.Comprobante_id = " . $r['id'])
                                                   ->fetch(PDO::FETCH_ASSOC);
                
                foreach($registros[$k]['Detalle'] as $k1 => $d)
                {
                    $registros[$k]['Detalle'][$k1]['Producto'] = $this->pdo->query("SELECT * FROM producto p WHERE p.id = " . $d['Producto_id'])
                                                                      ->fetch(PDO::FETCH_ASSOC);
                }
            }
            
            return $anexGrid->responde($registros, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM comprobante WHERE id = ?");
			$stm->execute(array($id));
            
			$c = $stm->fetch(PDO::FETCH_OBJ);
            
            /* El cliente asignado */
            $c->{'Cliente'} = $this->pdo->query("SELECT * FROM cliente c WHERE c.id = " . $c->Cliente_id)
                                        ->fetch(PDO::FETCH_OBJ);

            /* Traemos el detalle */
            $c->{'Detalle'} = $this->pdo->query("SELECT * FROM comprobante_detalle cd WHERE cd.Comprobante_id = " . $c->id)
                                        ->fetchAll(PDO::FETCH_OBJ);

            foreach($c->Detalle as $k => $d)
            {
                $c->Detalle[$k]->{'Producto'} = $this->pdo->query("SELECT * FROM producto p WHERE p.id = " . $d->Producto_id)
                                                          ->fetch(PDO::FETCH_OBJ);
            }
            
            return $c;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM comprobante WHERE id = ?");
			$stm->execute(array($id));
		}
        catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	
    public function Actualizar(Produccion $data)
    {
        try 
        {
           $descripcion = $data->__GET('descripcion');
           $estado = $data->__GET('estado');
           
           $id2 = $data->__GET('id2');

            foreach($descripcion as $index => $value)
            {
                
                $sql = "UPDATE comprobante_detalle SET
                        descripcion        = ?
                        
                    WHERE Producto_id = ? AND Comprobante_id = ?";

            $this->pdo->prepare($sql)
                 ->execute(
                array(

                    $descripcion[$index],
                    $id2[$index],
                    $data->__GET('id')
                    )
                );
            }
            foreach($estado as $index => $value)
            {
                
                $sql = "UPDATE comprobante_detalle SET
                        estado        = ?
                        
                    WHERE  Producto_id = ? AND Comprobante_id = ?";
                
                $this->pdo->prepare($sql)
                          ->execute(
                            array(
                                $estado[$index],
                                $id2[$index],
                                $data->__GET('id')
                            ));
            }
            
            if ($data->__GET('foto') !== null) {
                foreach($_FILES['foto']['error'] as $key => $error)
            {
            $foto1 =$_FILES['foto']['name'][$key];
            move_uploaded_file ($_FILES['foto']['tmp_name'][$key], 'uploads/' . $foto1);
            
            
            }
            foreach($_FILES['foto']['name'] as $index => $value)
            {
                $foto1 =$_FILES['foto']['name'];
                $sql = "UPDATE comprobante_detalle SET
                        foto        = ?
                        
                    WHERE  Producto_id = ? AND Comprobante_id = ?";
                
                $this->pdo->prepare($sql)
                          ->execute(
                            array(
                                $foto1[$index],
                                $id2[$index],
                                $data->__GET('id')
                            ));
            }
            }
            

            return true;
        }
        catch (Exception $e) 
        {
            return false;
        }
    }
   /* public function Actualizar(Personal $data)
    {
        try 
        {
            $sql = "UPDATE produccion SET
                        cedula          = ?, 
                        nombre          = ?, 
                        apellidos       = ?,
                        cargo           = ?,
                        sexo            = ?, 
                        f_ingreso       = ?,
                        f_egreso        = ?,
                        foto            = ?
                    WHERE id = ?";

            $this->pdo->prepare($sql)
                 ->execute(
                array(
                    $data->__GET('cedula'),
                    $data->__GET('nombre'), 
                    $data->__GET('apellidos'),
                    $data->__GET('cargo'), 
                    $data->__GET('sexo'),
                    $data->__GET('f_ingreso'),
                    $data->__GET('f_egreso'),
                    $data->__GET('foto'),
                    $data->__GET('id')
                    )
                );
        } catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }*/
}