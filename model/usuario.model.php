<?php
require_once 'lib/anexgrid.php';

class UsuarioModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = new PDO('mysql:host=localhost;dbname=factura;charset=utf8', 'root', '');
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		        
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /* Anex Grid */
            $anexgrid = new AnexGrid();

            /* Si es que hay filtro, tenemos que crear un WHERE dinámico */
		    $wh = "id > 0";
		    
		    foreach($anexgrid->filtros as $f)
		    {
		    	if($f['columna'] == 'cedula') $wh .= " AND cedula LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'nombre') $wh .= " AND nombre LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'apellido') $wh .= " AND apellido LIKE '%" . addslashes ($f['valor']) . "%'";
		        
		    }
            /* Los registros */
            
            $sql = "
                SELECT * FROM usuario
                WHERE $wh ORDER BY $anexgrid->columna $anexgrid->columna_orden
                LIMIT $anexgrid->pagina, $anexgrid->limite
            ";

			$stm = $this->pdo->prepare( $sql );
			$stm->execute();
            
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            
            /* El total de registros */
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM usuario
                WHERE $wh
            ")->fetchObject()->Total;
            

			return $anexgrid->responde($result, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM usuario WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Usuario();

			$alm->__SET('id', $r->id);
			$alm->__SET('cedula', $r->cedula);
			$alm->__SET('nombre', $r->nombre);
			$alm->__SET('apellido', $r->apellido);
			$alm->__SET('password', $r->password);
			$alm->__SET('passwordr', $r->passwordr);
			$alm->__SET('fecha', $r->fecha);
			$alm->__SET('nivel', $r->nivel);
			
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM usuario WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Usuario $data)
	{
	
		
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM usuario WHERE id = ?");
			          

			$stm->execute(array(
				$data->__GET('id')

				));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Usuario();

			
			$alm->__SET('passwordr', $r->passwordr);
			
			if ($data->__GET('passwordr') == $alm->__GET('passwordr')) {
				$sql = "UPDATE usuario SET
						cedula		    = ?, 
						nombre		    = ?, 
						apellido		= ?, 
						password        = ?, 
						passwordr       = ?,
						fecha   		= ?,
						nivel 			= ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('cedula'),
					$data->__GET('nombre'),
					$data->__GET('apellido'),
					$data->__GET('password'),
					$data->__GET('password'), 
					$data->__GET('fecha'),
					$data->__GET('nivel'),
					$data->__GET('id')
					)
				);
			}
			
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Usuario $data)
	{
		try 
		{
		$sql = "INSERT INTO usuario (cedula,nombre,apellido,password,passwordr,fecha,nivel) 
		        VALUES (?, ?, ?, ?, ?, ?,?)";
		        if ($data->__GET('password') == $data->__GET('passwordr')) {
		        	# code...
		        	$this->pdo->prepare($sql)
				     ->execute(
					array(
							$data->__GET('cedula'),
							$data->__GET('nombre'),
							$data->__GET('apellido'),
							$data->__GET('password'),
							$data->__GET('passwordr'),
							$data->__GET('fecha'),
							$data->__GET('nivel')
						)
					);
		        }
		
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}