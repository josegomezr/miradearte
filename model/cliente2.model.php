<?php
require_once 'lib/anexgrid.php';

class ClienteModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = new PDO('mysql:host=localhost;dbname=factura;charset=utf8', 'root', '');
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		        
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /* Anex Grid */
            $anexgrid = new AnexGrid();

            /* Si es que hay filtro, tenemos que crear un WHERE dinámico */
		    $wh = "id > 0";
		    
		    foreach($anexgrid->filtros as $f)
		    {
		    	if($f['columna'] == 'RUC') $wh .= " AND RUC LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'Nombre') $wh .= " AND CONCAT(Nombre, ' ', apellido) LIKE '%" . addslashes ($f['valor']) . "%'";
		        
		    }
            /* Los registros */
            
            $sql = "
                SELECT * FROM cliente
                WHERE $wh ORDER BY $anexgrid->columna $anexgrid->columna_orden
                LIMIT $anexgrid->pagina, $anexgrid->limite
            ";

			$stm = $this->pdo->prepare( $sql );
			$stm->execute();
            
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            
            /* El total de registros */
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM cliente
                WHERE $wh
            ")->fetchObject()->Total;
            

			return $anexgrid->responde($result, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM cliente WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Cliente();

			$alm->__SET('id', $r->id);
			$alm->__SET('RUC', $r->RUC);
			$alm->__SET('Nombre', $r->Nombre);
			$alm->__SET('apellido', $r->apellido);
            $alm->__SET('tlf', $r->tlf);
			$alm->__SET('email', $r->email);
			$alm->__SET('Direccion', $r->Direccion);
			
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM cliente WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Cliente $data)
	{
		try 
		{
			$sql = "UPDATE cliente SET
						RUC 			= ?, 
						Nombre          = ?, 
						apellido        = ?,
						tlf     		= ?,
						email           = ?, 
						Direccion  		= ?
                        
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('RUC'),
					$data->__GET('Nombre'), 
					$data->__GET('apellido'),
					$data->__GET('tlf'), 
					$data->__GET('email'),
					$data->__GET('Direccion'),
                    $data->__GET('id')
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Cliente $data)
	{
		try 
		{
		$sql = "INSERT INTO cliente (RUC,Nombre,apellido,tlf,email,Direccion) 
		        VALUES (?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
			array(
					$data->__GET('RUC'),
					$data->__GET('Nombre'), 
					$data->__GET('apellido'),
					$data->__GET('tlf'), 
					$data->__GET('email'),
					$data->__GET('Direccion')
                    
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}