<?php
require_once 'lib/anexgrid.php';

class GastosModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = new PDO('mysql:host=localhost;dbname=factura;charset=utf8', 'root', '');
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		        
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /* Anex Grid */
            $anexgrid = new AnexGrid();

            /* Si es que hay filtro, tenemos que crear un WHERE dinámico */
		    $wh = "id > 0";
		    
		    foreach($anexgrid->filtros as $f)
		    {
		    			 
		        if($f['columna'] == 'rif') $wh .= " AND rif LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'cliente') $wh .= " AND cliente LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'nfactura') $wh .= " AND nfactura LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'fecha') $wh .= " AND fecha LIKE '%" . addslashes ($f['valor']) . "%'";
				if($f['columna'] == 'total') $wh .= " AND total LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'tipopago') $wh .= " AND tipopago LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'clasificacion') $wh .= " AND clasificacion LIKE '%" . addslashes ($f['valor']) . "%'";
		        if($f['columna'] == 'descripcion') $wh .= " AND descripcion LIKE '%" . addslashes ($f['valor']) . "%'";

		    }
            /* Los registros */
            
            $sql = "
                SELECT * FROM gastos
                WHERE $wh ORDER BY $anexgrid->columna $anexgrid->columna_orden
                LIMIT $anexgrid->pagina, $anexgrid->limite
            ";

			$stm = $this->pdo->prepare( $sql );
			$stm->execute();
            
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            
            /* El total de registros */
            $total = $this->pdo->query("
                SELECT COUNT(*) Total
                FROM gastos
                WHERE $wh
            ")->fetchObject()->Total;
            

			return $anexgrid->responde($result, $total);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM gastos WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Gastos();


			$alm->__SET('id', $r->id);
			$alm->__SET('rif', $r->rif);
			$alm->__SET('cliente', $r->cliente);
            $alm->__SET('nfactura', $r->nfactura);
			$alm->__SET('fecha', $r->fecha);
			$alm->__SET('total', $r->total);
			$alm->__SET('tipopago', $r->tipopago);
			$alm->__SET('clasificacion', $r->clasificacion);
			$alm->__SET('descripcion', $r->descripcion);
			
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM gastos WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Gastos $data)
	{
		try 
		{
			$sql = "UPDATE gastos SET
						 
						rif     		 = ?,
						cliente          = ?, 
						nfactura  		 = ?,
						fecha		     = ?,
						total		     = ?,
						tipopago		 = ?,
						clasificacion	 = ?,
						descripcion		 = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('rif'),
					$data->__GET('cliente'), 
					$data->__GET('nfactura'),
					$data->__GET('fecha'),
					$data->__GET('total'),
					$data->__GET('tipopago'),
					$data->__GET('clasificacion'),
					$data->__GET('descripcion'),
					$data->__GET('id')
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	

	public function Registrar(Gastos $data)
	{
		try 
		{
		$sql = "INSERT INTO gastos (rif,cliente,nfactura,fecha,total,tipopago,clasificacion,descripcion) 
		        VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
			array(
					$data->__GET('rif'),
					$data->__GET('cliente'),
					$data->__GET('nfactura'),
					$data->__GET('fecha'),
                    $data->__GET('total'),
                    $data->__GET('tipopago'),
                    $data->__GET('clasificacion'),
                    $data->__GET('descripcion')
                    
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}