<?php
require_once 'lib/anexgrid.php';
session_start();
class SesionModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
            $this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM usuario WHERE id = ?");
			          

			$stm->execute(array($id));
			$r = $stm->fetch(PDO::FETCH_OBJ);

			$alm = new Sesion();

			$alm->__SET('id', $r->id);
			$alm->__SET('cedula', $r->usuario);
			$alm->__SET('password', $r->password);
		
			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("DELETE FROM usuario WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Sesion $data)
	{
		try 
		{
			$sql = "UPDATE usuario SET
						 
						cedula          = ?, 
						password        = ?
						
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('cedula'),
					$data->__GET('password')
					
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Comprobar(Sesion $data)
	{
		try 
		{

			$sql ="SELECT * FROM usuario WHERE cedula = ? AND password = ?";
 			$query = $this->pdo->prepare($sql);
 			$query->execute(
			array(
					$data->__GET('cedula'),
					$data->__GET('password') 
					
				)
			);
			$this->pdo = null;
			//si existe el usuario
			if($query->rowCount() == 1)
			{
				 
				 $fila  = $query->fetch();
				 $_SESSION['cedula'] = $fila['cedula'];
				 $_SESSION['nombre'] = $fila['nombre'];
				 $_SESSION['nivel'] = $fila['nivel'];			 
				 return  header('Location: ?c=Comprobante&a=Crud');
	
			}else

			echo '<script language=javascript>
			alert("los datos introducidos son incorrectos")
			self.location = "index.php"
			</script>';

		     
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}