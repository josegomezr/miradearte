<h1 class="page-header"><i class="glyphicon glyphicon-inbox"></i> Inventario</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Inventario&a=Crud">Nuevo Producto <i class="glyphicon glyphicon-inbox"></i></a>
</div>

<div id="list"></div>

<script>
    $(document).ready(function(){
        var agrid = $("#list").anexGrid({
            class: 'table-striped table-bordered table-condensed',
            columnas: [  
                { leyenda: 'Nombre Producto', style: 'width:200px;' ,ordenable: true, columna: 'Nombre', filtro: true },
                { leyenda: 'Descripcion', ordenable: true, style: 'width:200px;', columna: 'descripcion'},
                { leyenda: 'Cantidad Inventario', style: 'width:120px;', ordenable: true, columna: 'cantidad', filtro: true },
                { leyenda: 'Fecha Entrada', columna: 'f_entrada', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Fecha Salida', columna: 'f_salida', style: 'width:120px;', ordenable: true, filtro: true},
                { leyenda: 'Color', columna: 'color', style: 'width:130px;', ordenable: true, filtro: true},
                { leyenda: 'Precio Venta', columna: 'Precio', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Precio Compra', columna: 'precio_c', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'N° de Factura', columna: 'nfactura', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Proveedor', columna: 'proveedor', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Editar', style: 'width:40px' },
                { leyenda: 'Eliminar', style: 'width:40px' }
            ],
            modelo: [
                { propiedad: 'Nombre', formato: function(tr, obj, celda){
                    return obj.Nombre;
                }},
                { propiedad: 'descripcion' },
                { propiedad: 'cantidad' }, 
                { propiedad: 'f_entrada'},
                { propiedad: 'f_salida' },
                { propiedad: 'color' },
                { propiedad: 'Precio' },
                { propiedad: 'precio_c' },
                { propiedad: 'nfactura' },
                { propiedad: 'proveedor' },
                { class: 'text-center', formato: function(tr, obj, valor){
                    return anexGrid_dropdown({
                        contenido: '<i class="glyphicon glyphicon-cog"></i>',
                        class: 'btn btn-primary btn-xs',
                        target: '_blank',
                        id: 'editar-' + obj.id,
                        data: [
                            { href: '?c=Inventario&a=CrudEntrada&id=' + obj.id, contenido: 'Entrada Producto' },
                            { href: '?c=Inventario&a=CrudSalida&id=' + obj.id, contenido: 'Salida Producto' }
                        ]
                    });
                    }
                },
                /*{ formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Inventario&a=Crud&id=' + obj.id
                    });    
                }},*/
                { formato: function(tr, obj, celda){
                    return anexGrid_boton({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        value: tr.data('fila')
                    });    
                }},
            ],
            url: '?c=Inventario&a=Listar',
            paginable: true,
            filtrable: true,
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC'
        })
        
        agrid.tabla().on('click', '.btn-eliminar', function(){
            if(!confirm('¿Esta seguro de eliminar este registro?')) return;
            
            /* Obtiene el objeto actual de la fila seleccionada */
            var fila = agrid.obtener($(this).val());
            
            /* Petición ajax al servidor */
            $.post('?c=Inventario&a=Eliminar', {
                id: fila.id
            }, function(r){
                if(r) agrid.refrescar();
            }, 'json')
            
            return false;
        })
    })
</script>
