<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('nombre') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Inventario">Inventario</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('nombre') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-inventario" action="?c=Inventario&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>Nombre del Producto</label>
        <input type="text" name="Nombre" value="<?php echo $alm->__GET('Nombre'); ?>" class="form-control" placeholder="Ingrese Nombre del Producto" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
	<div class="col-md-4">
    <div class="form-group">
        <label>Cantidad Entrante</label>
        <input type="text" name="cantidad" value="<?php echo $alm->__GET('cantidad'); ?>" class="form-control" placeholder="Ingrese Cantidad a Registrar" data-validacion-tipo="requerido|numero|min:1" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Fecha de Ingreso</label>
        <input  type="date" name="f_entrada" value="<?php echo $alm->__GET('f_entrada'); ?>" class="form-control" placeholder="Ingrese su fecha de ingreso" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Color</label>
        <input  type="text" name="color" value="<?php echo $alm->__GET('color'); ?>" class="form-control" placeholder="Ingrese Color del Producto" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Precio Compra</label>
        <input  type="text" name="precio_c" value="<?php echo $alm->__GET('precio_c'); ?>" class="form-control" placeholder="Ingrese Precio de Compra" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Precio Venta</label>
        <input  type="text" name="Precio" value="<?php echo $alm->__GET('Precio'); ?>" class="form-control" placeholder="Ingrese Precio de Venta" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Proveedor</label>
        <select class="form-control" name="proveedor" data-validacion-tipo="requerido">
          <option value="<?php echo $alm->__GET('proveedor'); ?>"><?php echo $alm->__GET('proveedor'); ?></option> 
          <?php 
                        $con=mysql_connect("localhost","root",""); 
                        mysql_select_db("factura", $con);
                        $re=mysql_query("SELECT nombreempresa FROM proveedor ORDER BY id DESC");
                        while($f=mysql_fetch_array($re))
                        { 
                        ?>
                            <option value="<?php echo $f['nombreempresa']; ?>"><?php echo $f['nombreempresa']; ?></option>
                    
                <?php
                }
                ?>
        </select>
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Descripción</label>
        <textarea name="descripcion" value="<?php echo $alm->__GET('descripcion'); ?>" class="form-control" placeholder="Ingrese Descripción del  Producto" rows="1" ></textarea>
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>N° de Factura</label>
        <textarea name="nfactura" value="<?php echo $alm->__GET('nfactura'); ?>" class="form-control" placeholder="Ingrese el N° de Factura" rows="1" ></textarea>
    </div>
    </div>
 <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-inventario").submit(function(){
            return $(this).validate();
        });
    })
</script>