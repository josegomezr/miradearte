<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('Nombre') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Inventario">Inventario</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('Nombre') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-inventario" action="?c=Inventario&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>Nombre del Producto</label>
        <input readonly type="text" name="Nombre" value="<?php echo $alm->__GET('Nombre'); ?>" class="form-control" placeholder="Ingrese Nombre del Producto" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
	<div class="col-md-4">
    <div class="form-group">
        <label>Cantidad Entrante</label>
        <input type="text" name="cantidad" value="" class="form-control" placeholder="Ingrese Cantidad a Registrar" data-validacion-tipo="requerido|numero|min:1" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Fecha de Ingreso</label>
        <input  type="date" name="f_entrada" value="<?php echo $alm->__GET('f_entrada'); ?>" class="form-control" placeholder="Ingrese su fecha de ingreso" data-validacion-tipo="requerido" />
    </div>
    </div>
 <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-inventario").submit(function(){
            return $(this).validate();
        });
    })
</script>