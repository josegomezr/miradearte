<h1 class="page-header">
    Producción
</h1>

<div id="list"></div>

<script>
    $(document).ready(function(){
        $("#list").anexGrid({
            class: 'table-striped table-bordered',
            columnas: [
                { leyenda: 'Cliente', style: 'width:100px;', columna: 'Cliente_id', ordenable: true },
                { leyenda: 'IVA', style: 'width:100px;', columna: 'IGV', ordenable: true  },
                { leyenda: 'Sub Total', style: 'width:100px;', columna: 'SubTotal', ordenable: true  },
                { leyenda: 'Total', style: 'width:100px;', columna: 'Total', ordenable: true  },
            ],
            modelo: [
                {formato: function(tr, obj, valor){
                    return anexGrid_link({
                        href: '?c=produccion&a=ver&id=' + obj.id,
                        contenido: obj.Cliente.Nombre
                    });
                }},
                { propiedad: 'IGV', class: 'text-right', },
                { propiedad: 'SubTotal', class: 'text-right', },
                { propiedad: 'Total', class: 'text-right', },
               /*{ formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Produccion&a=Ver2&id=' + obj.id
                    });    
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        href: '?c=comprobante&a=Eliminar&id=' + obj.id
                    });    
                }},*/
            ],

            url: '?c=Produccion&a=Listar',
            limite: 10,
            columna: 'id',
            
            columna_orden: 'DESC',
            paginable: true
        });


    })
</script>