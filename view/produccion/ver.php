<div class="row">
<fieldset>
    <legend>Datos del cliente</legend>
    <form id="frm-personal" action="?c=Produccion&a=Guardar2" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $comprobante->id; ?>" />
    <div class="col-md-3">
    <div class="form-group">
        <label>Cedula</label>
        <input readonly type="text" name="cedula" value="<?php echo $comprobante->Cliente->RUC; ?>" class="form-control" placeholder="Ingrese su cedula" data-validacion-tipo="requerido|min:8|numero" />
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
        <label>Nombre</label>
        <input readonly type="text" name="nombre" value="<?php echo $comprobante->Cliente->Nombre; ?>" class="form-control" placeholder="Ingrese su nombre" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
        <label>Apellido</label>
        <input readonly type="text" name="apellido" value="<?php echo $comprobante->Cliente->apellido; ?>" class="form-control" placeholder="Ingrese sus apellidos" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
        <label>Telefono</label>
        <input readonly type="text" name="telefono" value="<?php echo $comprobante->Cliente->tlf; ?>" class="form-control" placeholder="Ingrese su cargo" data-validacion-tipo="requerido|min:5" />
    </div>
    </div>
    <div class="col-md-3">    
    <div class="form-group">
        <label>Dirección</label>
        <input readonly type="text" name="direccion" value="<?php echo $comprobante->Cliente->Direccion; ?>" class="form-control " placeholder="Ingrese su fecha de ingreso" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
        <label>Email</label>
        <input readonly type="email" name="email" value="<?php echo $comprobante->Cliente->email; ?>" class="form-control " placeholder="Ingrese su fecha de egreso" data-validacion-tipo="requerido" />
    </div>
    </div>
     
    </fieldset>
    <fieldset>
    <legend>Datos del Producto</legend>
    <?php foreach($comprobante->Detalle as $d): ?>
    <div class="col-md-3">
    <div class="form-group">
        <label>Producto</label>
        
        <input readonly type="text" name="producto[]" value="<?php echo $d->Producto->Nombre." ".$d->Producto->color; ?>" class="form-control " />
        
    </div>
    </div>
    <div class="col-md-1">
    <div class="form-group">
        <label>Cantidad</label>
        
        
        <input readonly type="text" name="cantidad[]" value="<?php echo $d->Cantidad; ?>" class="form-control " placeholder="Ingrese su fecha de egreso" />
        
    </div>
    </div>
    <div class="col-md-3">
    <div class="form-group">
        <label>Descripcion del diseño</label>
        
        
        
        <textarea type="text" name="descripcion[]" class="form-control"><?php echo $d->descripcion; ?></textarea>
    </div>
    </div>
    <div class="col-md-2">
    <div class="form-group">
        <label>Estado de la produccion</label>
        <select name="estado[]" class="form-control">
            <option value="<?php echo $d->estado; ?>"><?php echo $d->estado; ?></option>
            <option value="Por Hacer">Por hacer</option>
            <option value="En Producción">En producción</option>
            <option value="Terminado">Terminado</option>
        </select>
    </div>
    </div>
    <div class="col-md-2">
            <div class="form-group">
                <label>Foto del diseño</label>
                <input type="hidden" name="foto[]" value="<?php echo $d->foto; ?>" />
                <input type="file" name="foto[]" placeholder="Ingrese una imagen" />
            </div>     
        </div>
        <div class="col-md-3">
            <div class="form-group">
            <?php if($d->foto != ''): ?>
                <div class="img-thumbnail text-center">
                    <img src="uploads/<?php echo $d->foto; ?>" style="width:50%;" />
                </div>
            <?php endif; ?> 
            </div>           
        </div>
        
        <input type="hidden" name="id2[]" value="<?php echo $d->Producto->id; ?>" /> 
    </fieldset>
    <?php endforeach; ?>


    <hr>
    <br>
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>

            
                