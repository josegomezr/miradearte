<form id="frm-comprobante" method="post" action="?c=Comprobante&a=Guardar">
    <div class="row">
        <div class="col-xs-12">

            <fieldset>
                <legend>PEDIDOS | Datos del cliente</legend>
                <div class="row">
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input autocomplete="off" id="ruc" class="form-control" type="text" placeholder="Cedula" />                    
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Cliente</label>
                            <input autocomplete="off" id="cliente" disabled class="form-control" type="text" placeholder="Ingrese el nombre del cliente" />
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Telefono</label>
                            <input autocomplete="off" id="telefono" disabled class="form-control" type="text" placeholder="Ingrese el nombre del cliente" />
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Email</label>
                            <input autocomplete="off" id="email" disabled class="form-control" type="text" placeholder="Ingrese el nombre del cliente" />
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input autocomplete="off" id="direccion" disabled class="form-control" type="text" placeholder="Dirección" />                    
                        </div>
                        </div>
                        <div class="col-xs-6">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <input autocomplete="off" id="vendedor" disabled class="form-control" type="text" placeholder="Vendedor" value="<?php echo $_SESSION['nombre']; ?>" />                    
                        </div>
                    </div>
                    
                </div>
            </fieldset>

            <div class="well well-sm">
            <legend>Productos</legend>
                <div class="row">
                    <div class="col-xs-3">
                        <input id="producto_id" type="hidden" value="0" />
                        <input autocomplete="off" id="producto" class="form-control" type="text" placeholder="Nombre del producto" />
                    </div>
                   
                    <div class="col-xs-2">
                        <input autocomplete="off" id="color" disabled class="form-control" type="text" placeholder="Color" />
                    </div>
                     <div class="col-xs-2">
                        <input autocomplete="off" id="cantidad" class="form-control" type="text" placeholder="Cantidad" />
                    </div>
                    <div class="col-xs-2">
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">Bsf</span>
                          <input autocomplete="off" id="precio" class="form-control" type="text" placeholder="Precio" />
                        </div>
                    </div>
                    <div class="col-xs-1">
                        <button class="btn btn-primary form-control" id="btn-agregar" type="button">
                             <i class="glyphicon glyphicon-plus"></i>
                        </button>
                    </div>
                </div>            
            </div>

            <hr />

            <ul id="facturador-detalle" class="list-group"></ul>
            
            <button class="btn btn-primary btn-block btn-lg" type="submit">Generar Factura</button>

        </div>
</div>    
</form>

<script id="facturador-detalle-template" type="text/x-jsrender" src="">
    {{for items}}
    <li class="list-group-item">
        <div class="row">
            <div class="col-xs-6">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-danger form-control" onclick="facturador.retirar({{:id}});">
                            <i class="glyphicon glyphicon-minus"></i>
                        </button>
                    </span>
                    <input name="producto_id" type="hidden" value="{{:producto_id}}" />
                    <input disabled name="producto" class="form-control" type="text" placeholder="Nombre del producto" value="{{:producto}}" />
                </div>
            </div>
            <div class="col-xs-2">
                <input name="cantidad" class="form-control" type="text" placeholder="Cantidad" value="{{:cantidad}}" />
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">Bsf.</span>
                  <input name="precio" class="form-control" type="text" placeholder="Precio" value="{{:precio}}" />
                </div>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                    <span class="input-group-addon">Bsf.</span>
                    <input name="precio"  class="form-control" type="text" readonly value="{{:total}}" />
                    <span class="input-group-btn">
<button type="button" class="btn btn-success form-control" onclick="facturador.actualizar({{:id}}, this);" class="btn-retirar">
    <i class="glyphicon glyphicon-refresh"></i>
</button>
                    </span>
                </div>
            </div>
        </div>
    </li>
    {{else}}
    <li class="text-center list-group-item">No se han agregado productos al detalle</li>
    {{/for}}

    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Sub Total
            </div>
            <div class="col-xs-2">
                <b>{{:subtotal}}</b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                IVA (12%)
            </div>
            <div class="col-xs-2">
                <b>{{:igv}}</b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Total
            </div>
            <div class="col-xs-2">
                <b>{{:total}}</b>
            </div>
        </div>
    </li>
</script>

<script src="assets/scripts/comprobante.js"></script>