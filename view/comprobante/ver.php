<style type="text/css" media="print">
@media print {
#boton {display:none;}
#menu {display:none;}
#pie {display:none;}
}
</style>
<div class="row">
        <div class="col-xs-12">

            <fieldset>
                <legend>Datos de nuestro cliente</legend>
                <ul id="facturador-detalle" class="list-group">
                <li class="list-group-item">
                <div class="row">
                <div class="col-xs-2">
                        <div class="form-group">
                            <label>Cedula</label>
                            <p><?php echo $comprobante->Cliente->RUC; ?></p>                    
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Cliente</label>
                            <p><?php echo $comprobante->Cliente->Nombre." ".$comprobante->Cliente->apellido; ?></p>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Telefono</label>
                            <p><?php echo $comprobante->Cliente->tlf; ?></p>                    
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Email</label>
                            <p><?php echo $comprobante->Cliente->email; ?></p>                    
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Dirección</label>
                            <p><?php echo $comprobante->Cliente->Direccion; ?></p>                    
                        </div>
                    </div>
                </div>
                </li>
                </ul>
            </fieldset>
            <ul id="facturador-detalle" class="list-group">
            <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-7">
                            <label>Producto</label>
                        </div>
                        <div class="col-xs-1 text-right">
                            <label>Cantidad</label>
                        </div>
                        <div class="col-xs-2 text-right">
                            <label>Precio Unitario</label>
                        </div>
                        <div class="col-xs-2 text-right">
                            <label>Total</label>
                        </div>
                    </div>
                </li>
            
                <?php foreach($comprobante->Detalle as $d): ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-7">
                            <?php echo $d->Producto->Nombre." ".$d->Producto->color; ?>
                        </div>
                        <div class="col-xs-1 text-right">
                            <?php echo $d->Cantidad; ?>
                        </div>
                        <div class="col-xs-2 text-right">
                            <?php echo number_format($d->PrecioUnitario, 2); ?>
                        </div>
                        <div class="col-xs-2 text-right">
                            <?php echo number_format($d->Total, 2); ?>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            Sub Total
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->SubTotal, 2); ?></b>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            IVA (12%)
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->IGV, 2); ?></b>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            Total <b>(Bsf.)</b>
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->Total, 2); ?></b>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
</div>

<a class="btn btn-primary  btn-lg btn-block"  onclick="window.print();" id="boton">IMPRIMIR</a>