<h1 class="page-header">
    <a class="btn btn-primary pull-right btn-lg" href="?c=Comprobante&a=crud">Nuevo Pedido</a>
    Facturas
</h1>

<div id="list"></div>

<script>
    $(document).ready(function(){
        $("#list").anexGrid({
            class: 'table-striped table-bordered',
            columnas: [
                { leyenda: 'Cliente', style: 'width:150px;', columna: 'Cliente_id', ordenable: true },
                { leyenda: 'Fecha', style: 'width:150px;', columna: 'fecha', ordenable: true, filtro: true },
                { leyenda: 'Estado Facturas', style: 'width:100px;', columna: 'estado_f', ordenable: true },
                { leyenda: 'IVA', style: 'width:100px;', columna: 'IGV', ordenable: true  },
                { leyenda: 'Sub Total', style: 'width:100px;', columna: 'SubTotal', ordenable: true  },
                { leyenda: 'Total', style: 'width:100px;', columna: 'Total', ordenable: true  },
               
                { leyenda: 'Eliminar', style: 'width:30px;', columna: 'Total', ordenable: false }
            ],
            modelo: [
                { formato: function(tr, obj, valor){
                    return anexGrid_link({
                        href: '?c=comprobante&a=ver&id=' + obj.id,
                        contenido: obj.Cliente.Nombre +' '+ obj.Cliente.apellido
                    });
                }},
                { propiedad: 'fecha', class: 'text-center', },
                { propiedad: 'estado_f', class: 'text-center', },
                { propiedad: 'IGV', class: 'text-right', },
                { propiedad: 'SubTotal', class: 'text-right', },
                { propiedad: 'Total', class: 'text-right', },
                /*{ formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Personal&a=Crud&id=' + obj.id
                    });    
                }},*/
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        href: '?c=comprobante&a=Eliminar&id=' + obj.id
                    });    
                }},
            ],

            url: '?c=comprobante&a=Listar',
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC',
            paginable: true
        });


    })
</script>