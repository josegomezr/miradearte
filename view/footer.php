            <hr />

            <div class="row" id="pie">
                <div class="col-xs-12">
                    <footer class="text-center well">
                        <p>Sistema desarrollado por <a href="http://geeksystemve.com.ve">GeekSystemVE</a></p>
                    </footer>                
                </div>    
            </div>
        </div>

        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/js/jquery.anexsoft-validator.js"></script>
        <script src="assets/js/jquery.anexgrid.js"></script>
        <script src="assets/js/js-render.js"></script>
        <script src="assets/js/ini.js"></script>
    </body>
</html>