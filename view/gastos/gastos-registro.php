<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('rif') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Gastos">Gastos</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('rif') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-gastos" action="?c=Gastos&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>Rif/Cedula</label>
        <input type="text" name="rif" value="<?php echo $alm->__GET('rif'); ?>" class="form-control" placeholder="Ingrese Rif/Cedula" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
	<div class="col-md-4">
    <div class="form-group">
        <label>Cliente</label>
        <input type="text" name="cliente" value="<?php echo $alm->__GET('cliente'); ?>" class="form-control" placeholder="Ingrese el Cliente" data-validacion-tipo="requerido|numero|min:1" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>N° de Factura</label>
        <input  type="text" name="nfactura" value="<?php echo $alm->__GET('nfactura'); ?>" class="form-control" placeholder="Ingrese su N° Factura" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Fecha</label>
        <input  type="date" name="fecha" value="<?php echo $alm->__GET('fecha'); ?>" class="form-control" placeholder="Ingrese la Fecha" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Total</label>
        <input  type="text" name="total" value="<?php echo $alm->__GET('total'); ?>" class="form-control" placeholder="Ingrese el Total" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Tipo de Pago</label>
        <select name="tipopago" autocomplete="off" id="tipopago" class="form-control">
                                <option value="<?php echo $alm->__GET('tipopago'); ?>"> <?php echo $alm->__GET('tipopago'); ?></option>
                                <option value="Efectivo">Efectivo</option>
                                <option value="Debito">Debito</option>
                                <option value="Credito">Credito</option>
                                <option value="Transferencia">Transferencia</option>
                                <option value="Cheque">Cheque</option>
                            </select>    
       
    </div>
    </div>
    <div class="col-md-6">    
    <div class="form-group">
        <label>Clasificación</label>
        <select class="form-control" name="clasificacion" data-validacion-tipo="requerido">
          <option value="<?php echo $alm->__GET('clasificacion'); ?>"><?php echo $alm->__GET('clasificacion'); ?></option> 
          <?php 
                        $con=mysql_connect("localhost","root",""); 
                        mysql_select_db("factura", $con);
                        $re=mysql_query("SELECT nombre FROM clasificacion ORDER BY id");
                        while($f=mysql_fetch_array($re))
                        { 
                        ?>
                            <option value="<?php echo $f['nombre']; ?>"><?php echo $f['nombre']; ?></option>
                    
                <?php
                }
                ?>
        </select>
    </div>
    </div>
    <div class="col-md-6">    
    <div class="form-group">
        <label>Descripción</label>
        <input  type="text" name="descripcion" value="<?php echo $alm->__GET('descripcion'); ?>" class="form-control" placeholder="Ingrese la Descripción" data-validacion-tipo="requerido" />
    </div>
    </div>
 <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-inventario").submit(function(){
            return $(this).validate();
        });
    })
</script>