<h1 class="page-header"><i class="glyphicon glyphicon-inbox"></i> Gastos</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Gastos&a=Crud">Nuevo Gasto <i class="glyphicon glyphicon-inbox"></i></a>
</div>

<div id="list"></div>

<script>
    $(document).ready(function(){
        var agrid = $("#list").anexGrid({
            class: 'table-striped table-bordered table-condensed',
            columnas: [  
                { leyenda: 'Rif/Cedula', style: 'width:200px;' ,ordenable: true, columna: 'rif', filtro: true },
                { leyenda: 'Cliente', ordenable: true, style: 'width:200px;', columna: 'cliente', filtro: true},
                { leyenda: 'N° Facturas', style: 'width:120px;', ordenable: true, columna: 'nfactura', filtro: true },
                { leyenda: 'Fecha', columna: 'fecha', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Total', columna: 'total', style: 'width:120px;', ordenable: true, filtro: true},
                { leyenda: 'Tipo de Pago', columna: 'tipopago', style: 'width:130px;', ordenable: true, filtro: true},
                { leyenda: 'Clasificacion', columna: 'clasificacion', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Descripcion', columna: 'descripcion', style: 'width:120px;', ordenable: true, filtro: true },
                { leyenda: 'Editar', style: 'width:40px' },
                { leyenda: 'Eliminar', style: 'width:40px' }
            ],
            modelo: [
                { propiedad: 'rif', formato: function(tr, obj, celda){
                    return obj.rif;
                }},
                { propiedad: 'cliente', formato: function(tr, obj, celda){
                    return obj.cliente;
                }},
                { propiedad: 'nfactura', formato: function(tr, obj, celda){
                    return obj.nfactura;
                }},
                { propiedad: 'fecha', formato: function(tr, obj, celda){
                    return obj.fecha;
                }},
                { propiedad: 'total', formato: function(tr, obj, celda){
                    return obj.total;
                }},
                { propiedad: 'tipopago', formato: function(tr, obj, celda){
                    return obj.tipopago;
                }},
                { propiedad: 'clasificacion', formato: function(tr, obj, celda){
                    return obj.clasificacion;
                }},
                { propiedad: 'descripcion', formato: function(tr, obj, celda){
                    return obj.descripcion;
                }},
              
                /*{ class: 'text-center', formato: function(tr, obj, valor){
                    return anexGrid_dropdown({
                        contenido: '<i class="glyphicon glyphicon-cog"></i>',
                        class: 'btn btn-primary btn-xs',
                        target: '_blank',
                        id: 'editar-' + obj.id,
                        data: [
                            { href: '?c=Inventario&a=CrudEntrada&id=' + obj.id, contenido: 'Entrada Producto' },
                            { href: '?c=Inventario&a=CrudSalida&id=' + obj.id, contenido: 'Salida Producto' }
                        ]
                    });
                    }
                },*/
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Gastos&a=Crud&id=' + obj.id
                    });    
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_boton({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        value: tr.data('fila')
                    });    
                }},
            ],
            url: '?c=Gastos&a=Listar',
            paginable: true,
            filtrable: true,
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC'
        })
        
        agrid.tabla().on('click', '.btn-eliminar', function(){
            if(!confirm('¿Esta seguro de eliminar este registro?')) return;
            
            /* Obtiene el objeto actual de la fila seleccionada */
            var fila = agrid.obtener($(this).val());
            
            /* Petición ajax al servidor */
            $.post('?c=Gastos&a=Eliminar', {
                id: fila.id
            }, function(r){
                if(r) agrid.refrescar();
            }, 'json')
            
            return false;
        })
    })
</script>
