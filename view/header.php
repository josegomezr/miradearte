<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Mira de Arte</title>
        
        <meta charset="utf-8" />
        
        <link rel="stylesheet" href="assets/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <script src="assets/js/jquery-1.11.3.js"></script>
	</head>
  <div>
  <div style="'width:100%;'"> <img src="assets/img/header2-2.jpg" class="img-responsive"></div>
    <ul class="nav nav-pills  container" id="menu">
                  <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="glyphicon glyphicon-inbox"></i> Inventario <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a href="?c=Inventario"> Registro de Producto</a></li>
                      <li role="separator" class="divider"></li>
                      <li role="presentation"><a href="?c=Proveedor"> Registro Proveerdor</a></li>
                    </ul>
                  </li>
                  <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-shopping-basket"></i> Ventas <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a href="?c=Comprobante&a=crud"> Pedidos</a></li>
                      <li role="separator" class="divider"></li>
                      <li role="presentation"><a href="?c=Comprobante"> Facturas</a></li>
                      <li role="separator" class="divider"></li>
                      <li role="presentation"><a href="?c=Produccion"> Producción</a></li>
                   </ul>
                  </li>
                  <?php
                  if(isset($_SESSION['nivel'] ) && $_SESSION['nivel'] =="Administrador"){
                  echo'
                  <li>
                    <li role="presentation"><a href="?c=Gastos"><i class="glyphicon glyphicon-list"></i> Gastos</a></li>
                  </li>';
                  }
                  ?>
                  
                  <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="glyphicon glyphicon-wrench"></i> Mantenimiento<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    <?php
                  if(isset($_SESSION['nivel'] ) && $_SESSION['nivel'] =="Administrador"){
                  echo'
                  <li>
                    <li role="presentation"><a href="?c=Usuario"> Registro de Usuarios</a></li>
                  </li>';
                  }
                  ?>

                      
                      <li role="separator" class="divider"></li>
                      <li role="presentation"><a href="?c=Cliente"> Registro de clientes</a></li>
                    </ul>
                  </li>
                  <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-shopping-file"></i> Reportes <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a href="../facturador/view/reportes/proveedores.php"><i class="glyphicon glyphicon-file"></i> Proveedores</a></li>
                      <li role="separator" class="divider"></li>
                      <li role="presentation"><a href="../facturador/view/reportes/gastos.php"><i class="glyphicon glyphicon-file"></i> Gastos</a></li>
                   </ul>
                  </li>
                  
                  
                  <ul class="nav navbar-right">
                  
                        <li><a href="index.php" onclick="return confirm('¿Está seguro de cerrar la sesión??');"><i class="glyphicon glyphicon-off"></i> Cerrar sesión</a></li>
                </ul>
            </ul>
  </div>
    <body>
        
        <div class="container">
            
           <!-- <h1 class="jumbotron">
                Ejemplo de facturado con PHP + JSRender
            </h1>-->
            

            <br>