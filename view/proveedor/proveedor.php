<h1 class="page-header"><i class="glyphicon glyphicon-user"></i> Proveedor</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Proveedor&a=Crud">Nuevo Proveedor <i class="glyphicon glyphicon-user"></i></a>
</div>

<div id="list"></div>

<script>
    $(document).ready(function(){
        var agrid = $("#list").anexGrid({
            class: 'table-striped table-bordered table-condensed',
            columnas: [
                
                { leyenda: 'Rif', ordenable: true, columna: 'rif', filtro: true },  
                { leyenda: 'Nombre E.', ordenable: true, columna: 'nombreempresa', filtro: true },
                { leyenda: 'Direccion', ordenable: true, columna: 'direccion', filtro: true },
                { leyenda: 'Tlf E.', ordenable: true, columna: 'tlfempresa', filtro: true },
                { leyenda: 'Email E.', ordenable: true, columna: 'emailempresa', filtro: true },
                { leyenda: 'Nombre y Apellido', ordenable: true, columna: 'nombrecontacto', filtro: true },
                { leyenda: 'Tlf C.', ordenable: true, columna: 'tlfcontacto', filtro: true },
                { leyenda: 'Email C.', ordenable: true, columna: 'emailcontacto', filtro: true },
                { leyenda: 'Editar', style: 'width:40px' },
                { leyenda: 'Eliminar', style: 'width:40px' }
            ],
            modelo: [
                { propiedad: 'rif', formato: function(tr, obj, celda){
                    return obj.rif;
                }},
                { propiedad: 'nombreempresa', formato: function(tr, obj, celda){
                    return obj.nombreempresa;
                }},
                { propiedad: 'direccion', formato: function(tr, obj, celda){
                    return obj.direccion;
                }},
                { propiedad: 'tlfempresa', formato: function(tr, obj, celda){
                    return obj.tlfempresa;
                }},
                { propiedad: 'emailempresa', formato: function(tr, obj, celda){
                    return obj.emailempresa;
                }},
                { propiedad: 'nombreempresa', formato: function(tr, obj, celda){
                    return obj.nombrecontacto + ' ' + obj.apellidocontacto;
                }},
                { propiedad: 'tlfcontacto', formato: function(tr, obj, celda){
                    return obj.tlfcontacto;
                }},
                { propiedad: 'emailcontacto', formato: function(tr, obj, celda){
                    return obj.emailcontacto;
                }},
                /*{ class: 'text-center', formato: function(tr, obj, valor){
                    return anexGrid_dropdown({
                        contenido: '<i class="glyphicon glyphicon-cog"></i>',
                        class: 'btn btn-primary btn-xs',
                        target: '_blank',
                        id: 'editar-' + obj.id,
                        data: [
                            { href: '?c=Personal&a=Crud&id=' + obj.id, contenido: 'Editar' },
                            { href: '?c=Asistencia&a=Crud&id=' + obj.id, contenido: 'Asistencia' },
                            { href: '?c=Pago&a=Crud&id=' + obj.id, contenido: 'Pago' }
                        ]
                    });
                    }
                },*/
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Proveedor&a=Crud&id=' + obj.id
                    });    
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_boton({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        value: tr.data('fila')
                    });    
                }},
            ],
            url: '?c=Proveedor&a=Listar',
            paginable: true,
            filtrable: true,
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC'
        })
        
        agrid.tabla().on('click', '.btn-eliminar', function(){
            if(!confirm('¿Esta seguro de eliminar este registro?')) return;
            
            /* Obtiene el objeto actual de la fila seleccionada */
            var fila = agrid.obtener($(this).val());
            
            /* Petición ajax al servidor */
            $.post('?c=Proveedor&a=Eliminar', {
                id: fila.id
            }, function(r){
                if(r) agrid.refrescar();
            }, 'json')
            
            return false;
        })
    })
</script>
