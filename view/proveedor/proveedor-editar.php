<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('nombreempresa') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Proveedor">Proveedor</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('nombreempresa') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-proveedor" action="?c=Proveedor&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>RIF/Cedula</label>
        <input type="text" name="rif" value="<?php echo $alm->__GET('rif'); ?>" class="form-control" placeholder="Ingrese RIF de la Empresa" data-validacion-tipo="requerido|min:8" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Nombre Empresa.</label>
        <input type="text" name="nombreempresa" value="<?php echo $alm->__GET('nombreempresa'); ?>" class="form-control" placeholder="Ingrese el Nombre de la Empresa" data-validacion-tipo="requerido|min:2" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Dirección</label>
        <input type="text" name="direccion" value="<?php echo $alm->__GET('direccion'); ?>" class="form-control" placeholder="Ingrese la Dirección"  />
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
        <label>Tlf Empresa</label>
        <input type="text" name="tlfempresa" value="<?php echo $alm->__GET('tlfempresa'); ?>" class="form-control" placeholder="Ingrese el Tlf de la Empresa" data-validacion-tipo="requerido|min:8|numero" />
    </div>
    </div>
	<div class="col-md-6">
    <div class="form-group">
        <label>Email Empresa</label>
        <input type="email" name="emailempresa" value="<?php echo $alm->__GET('emailempresa'); ?>" class="form-control" placeholder="Ingrese el email de la Empresa" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Cedula</label>
        <input type="text" name="cedula" value="<?php echo $alm->__GET('cedula'); ?>" class="form-control" placeholder="Ingrese la Cedula del Contacto" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Nombre Contacto</label>
        <input  type="text" name="nombrecontacto" value="<?php echo $alm->__GET('nombrecontacto'); ?>" class="form-control " placeholder="Ingrese el Nombre del Contacto" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Apellido Contacto</label>
        <input  type="text" name="apellidocontacto" value="<?php echo $alm->__GET('apellidocontacto'); ?>" class="form-control " placeholder="Ingrese el Apellido del Contacto" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
        <label>Tlf Contacto</label>
        <input  type="text" name="tlfcontacto" value="<?php echo $alm->__GET('tlfcontacto'); ?>" class="form-control " placeholder="Ingrese el Tlf del Contacto" data-validacion-tipo="requerido" />
    </div>
    </div>  
    <div class="col-md-6">
    <div class="form-group">
        <label>Email Contacto</label>
        <input  type="email" name="emailcontacto" value="<?php echo $alm->__GET('emailcontacto'); ?>" class="form-control " placeholder="Ingrese el Email del Contacto" data-validacion-tipo="requerido" />
    </div>
    </div>
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-personal").submit(function(){
            return $(this).validate();
        });
    })
</script>