<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('Nombre') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Cliente">Cliente</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('Nombre') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-cliente" action="?c=Cliente&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>RIF/Cedula</label>
        <input type="text" name="RUC" value="<?php echo $alm->__GET('RUC'); ?>" class="form-control" placeholder="Ingrese su cedula" data-validacion-tipo="requerido|min:8|numero" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="Nombre" value="<?php echo $alm->__GET('Nombre'); ?>" class="form-control" placeholder="Ingrese su nombre" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="apellido" value="<?php echo $alm->__GET('apellido'); ?>" class="form-control" placeholder="Ingrese su apellido" data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Telefono</label>
        <input type="text" name="tlf" value="<?php echo $alm->__GET('tlf'); ?>" class="form-control" placeholder="Ingrese su Telefono" data-validacion-tipo="requerido|min:5" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Correo Electronico</label>
        <input  type="email" name="email" value="<?php echo $alm->__GET('email'); ?>" class="form-control " placeholder="Ingrese su Correo Electronico" data-validacion-tipo="requerido" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Dirección</label>
        <input  type="text" name="Direccion" value="<?php echo $alm->__GET('Direccion'); ?>" class="form-control " placeholder="Ingrese su Dirección" data-validacion-tipo="requerido" />
    </div>
    </div>
    	
    
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-personal").submit(function(){
            return $(this).validate();
        });
    })
</script>