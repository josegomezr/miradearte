<h1 class="page-header"><i class="glyphicon glyphicon-user"></i> Cliente</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Cliente&a=Crud">Nuevo Cliente <i class="glyphicon glyphicon-user"></i></a>
</div>

<div id="list"></div>

<script>
    $(document).ready(function(){
        var agrid = $("#list").anexGrid({
            class: 'table-striped table-bordered table-condensed',
            columnas: [
                
                { leyenda: 'Cedula', style: 'width:150px;', ordenable: true, columna: 'RUC', filtro: true },  
                { leyenda: 'Nombre y Apellido', ordenable: true, columna: 'Nombre', filtro: true },
                { leyenda: 'Telefono', style: 'width:150px;', ordenable: true, columna: 'tlf' },
                { leyenda: 'Correo', columna: 'email', style: 'width:140px;', ordenable: true },
                { leyenda: 'Direccion', columna: 'Direccion', style: 'width:130px;', ordenable: true },
                { leyenda: 'Editar', style: 'width:40px' },
                { leyenda: 'Eliminar', style: 'width:40px' }
            ],
            modelo: [
                
                { propiedad: 'RUC', formato: function(tr, obj, celda){
                    return obj.RUC;
                }},
                { propiedad: 'Nombre', formato: function(tr, obj, celda){
                    return obj.Nombre + ' ' + obj.apellido;
                }},
                { propiedad: 'tlf', formato: function(tr, obj, celda){
                    return obj.tlf;
                }},
                { propiedad: 'email', formato: function(tr, obj, celda){
                    return obj.email;
                }},
                { propiedad: 'Direccion', formato: function(tr, obj, celda){
                    return obj.Direccion;
                }},
                
                /*{ class: 'text-center', formato: function(tr, obj, valor){
                    return anexGrid_dropdown({
                        contenido: '<i class="glyphicon glyphicon-cog"></i>',
                        class: 'btn btn-primary btn-xs',
                        target: '_blank',
                        id: 'editar-' + obj.id,
                        data: [
                            { href: '?c=Personal&a=Crud&id=' + obj.id, contenido: 'Editar' },
                            { href: '?c=Asistencia&a=Crud&id=' + obj.id, contenido: 'Asistencia' },
                            { href: '?c=Pago&a=Crud&id=' + obj.id, contenido: 'Pago' }
                        ]
                    });
                    }
                },*/
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Cliente&a=Crud&id=' + obj.id
                    });    
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_boton({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        value: tr.data('fila')
                    });    
                }},
            ],
            url: '?c=Cliente&a=Listar',
            paginable: true,
            filtrable: true,
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC'
        })
        
        agrid.tabla().on('click', '.btn-eliminar', function(){
            if(!confirm('¿Esta seguro de eliminar este registro?')) return;
            
            /* Obtiene el objeto actual de la fila seleccionada */
            var fila = agrid.obtener($(this).val());
            
            /* Petición ajax al servidor */
            $.post('?c=Cliente&a=Eliminar', {
                id: fila.id
            }, function(r){
                if(r) agrid.refrescar();
            }, 'json')
            
            return false;
        })
    })
</script>
