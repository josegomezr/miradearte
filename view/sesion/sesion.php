
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Rectificadora SanJosé</title>
        
        <meta charset="utf-8" />
        
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.theme.min.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        
        <script src="assets/js/jquery-1.11.3.js"></script>
        <style>
body  {
    background: url("assets/img/INICIO.jpg") no-repeat center center fixed;
    
    margin-top: 300px;
    margin-bottom: 50px;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;

}
</style>
    </head>
<body >
<br>


<div class="container">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4" id="login">
                <form class="form-signin" role="form" method="post" action="?c=Sesion&a=Iniciar">
                    <input type="text" class="form-control" placeholder="Cedula" name="cedula">
                    <input type="password" class="form-control" placeholder="Contraseña" name="password">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
 </div>



<script>
    $(document).ready(function(){
        $("#frm-sesion").submit(function(){
            return $(this).validate();
        });
    })
</script>

        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/js/jquery.anexsoft-validator.js"></script>
        <script src="assets/js/jquery.anexgrid.min.js"></script>
        <script src="assets/js/ini.js"></script>
    </body>