<h1 class="page-header"><i class="glyphicon glyphicon-user"></i> Usuario</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Usuario&a=Crud">Nuevo Usuario <i class="glyphicon glyphicon-user"></i></a>
</div>

<div id="list"></div>

<script>
    $(document).ready(function(){
        var agrid = $("#list").anexGrid({
            class: 'table-striped table-bordered table-condensed',
            columnas: [
                { leyenda: 'Cedula', ordenable: true, columna: 'cedula', filtro: true},  
                { leyenda: 'Nombre', ordenable: true, columna: 'nombre', filtro: true},
                { leyenda: 'Apellido', ordenable: true, columna: 'apellido', filtro: true},
                { leyenda: 'Fecha', ordenable: true, columna: 'fecha', style:'width:130px;' },
                { leyenda: 'Nivel', ordenable: true, columna: 'nivel' },
                { leyenda: 'Editar', style: 'width:40px' },
                { leyenda: 'Eliminar', style: 'width:40px' }
            ],
            modelo: [
                
                { propiedad: 'cedula', formato: function(tr, obj, celda){
                    return obj.cedula;
                }},
                { propiedad: 'nombre', formato: function(tr, obj, celda){
                    return obj.nombre;
                }},
                { propiedad: 'apellido', formato: function(tr, obj, celda){
                    return obj.apellido;
                }},
                { propiedad: 'fecha', formato: function(tr, obj, celda){
                    return obj.fecha;
                }},
                { propiedad: 'nivel', formato: function(tr, obj, celda){
                    return obj.nivel;
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_link({
                        class: 'btn-primary btn-xs btn-block',
                        contenido: '<i class="glyphicon glyphicon-cog">',
                        href: '?c=Usuario&a=Crud&id=' + obj.id
                    });    
                }},
                { formato: function(tr, obj, celda){
                    return anexGrid_boton({
                        class: 'btn-danger btn-xs btn-block btn-eliminar',
                        contenido: '<i class="glyphicon glyphicon-trash">',
                        value: tr.data('fila')
                    });    
                }},
            ],
            url: '?c=Usuario&a=Listar',
            paginable: true,
            filtrable: true,
            limite: 10,
            columna: 'id',
            columna_orden: 'DESC'
        })
        
        agrid.tabla().on('click', '.btn-eliminar', function(){
            if(!confirm('¿Esta seguro de eliminar este registro?')) return;
            
            /* Obtiene el objeto actual de la fila seleccionada */
            var fila = agrid.obtener($(this).val());
            
            /* Petición ajax al servidor */
            $.post('?c=Usuario&a=Eliminar', {
                id: fila.id
            }, function(r){
                if(r) agrid.refrescar();
            }, 'json')
            
            return false;
        })
    })
</script>
