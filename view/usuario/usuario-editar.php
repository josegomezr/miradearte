<h1 class="page-header">
    <?php echo $alm->__GET('id') != null ? $alm->__GET('nombre') : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Usuario">Usuario</a></li>
  <li class="active"><?php echo $alm->__GET('id') != null ? $alm->__GET('nombre') : 'Nuevo Registro'; ?></li>
</ol>
<div class="row">
	<form id="frm-usuario" action="?c=Usuario&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->__GET('id'); ?>" />
    <div class="col-md-4">
    <div class="form-group">
        <label>Cedula</label>
        <input type="text" name="cedula" value="<?php echo $alm->__GET('cedula'); ?>" class="form-control" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $alm->__GET('nombre'); ?>" class="form-control" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="apellido" value="<?php echo $alm->__GET('apellido'); ?>" class="form-control" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Contraseña</label>
        <input type="password" name="password" value="" class="form-control"  data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        <label>Repetir Contraseña / Contraseña anterior</label>
        <input type="password" name="passwordr" value="" class="form-control"  data-validacion-tipo="requerido|min:3" />
    </div>
    </div>
    <div class="col-md-4">    
    <div class="form-group">
        <label>Nivel de Seguridad</label>
        <select name="nivel" autocomplete="off" id="nivel" class="form-control">
                                <option value="<?php echo $alm->__GET('nivel'); ?>"> <?php echo $alm->__GET('nivel'); ?></option>
                                <option value="Administrador">Administrador</option>
                                <option value="Vendedor">Vendedor</option>
        </select>    
       
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
        
        <input type="hidden" name="fecha" value="<?php echo date("Y-m-d"); ?>" class="form-control"   />
    </div>
    </div>

    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar <i class="glyphicon glyphicon-check"></i></button>
    </div>
</form>
</div>


<script>
    $(document).ready(function(){
        $("#frm-usuario").submit(function(){
            return $(this).validate();
        });
    })
</script>